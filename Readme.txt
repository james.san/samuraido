Setting:

Midori was living with his wife in peacefull village in Edo period.
However an evil samurai captured Midori's wife and some other vilagers.
Armed with his sword he ventures onto quest to regain his lost love. 


Features: 

There are 3 paths in game total as of this moment. 

The game is short platformer that is tribute to one of my favourite cultures and it's warriors.
It is also reminescent of romantic tragedy tropes prevalent in medieval culture of Europe.

Enjoy!

Character Control:

LeftArrow-left
RightArrow-right
Space - jump
X - Activate switch
Z - SwipeSword

